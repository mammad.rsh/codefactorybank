
# DKB Code Factory Bank

A Project based on DKB Code Factory Coding Challenge.



## Tech Stack

**Server:** Spring Boot 2.6.5, Java 11


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/mammad.rsh/codefactorybank
```

Go to the project directory

```bash
  cd codefactorybank
```

Config environment variables

Start the server

```bash
  ./mvnw spring-boot:run
```


## Authors

- mammad.rsh@gmail.com

