package ir.morahimi061.codefactorybank;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.morahimi061.codefactorybank.dto.DepositDto;
import ir.morahimi061.codefactorybank.dto.NewAccountDto;
import ir.morahimi061.codefactorybank.dto.TransferDto;
import ir.morahimi061.codefactorybank.model.AccountType;
import ir.morahimi061.codefactorybank.model.BankAccount;
import ir.morahimi061.codefactorybank.model.Customer;
import ir.morahimi061.codefactorybank.model.exception.ErrorCode;
import ir.morahimi061.codefactorybank.service.CustomerService;
import ir.morahimi061.codefactorybank.service.BankAccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.containsString;

@SpringBootTest
@AutoConfigureMockMvc
public class BankAccountTests {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    ObjectMapper mapper;
    @Autowired
    CustomerService customerService;
    @Autowired
    BankAccountService bankAccountService;

    @Test
    public void shouldOpenNewAccount() throws Exception {
        Customer customer = customerService.insert(new Customer());
        NewAccountDto newAccountDto = new NewAccountDto();
        newAccountDto.setAccountType(AccountType.CHECKING_ACCOUNT);
        newAccountDto.setCustomerId(customer.getId());

        MockHttpServletRequestBuilder mockRequest = post("/bank-account/openAccount")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(newAccountDto));

        this.mockMvc.perform(mockRequest).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("CHECKING_ACCOUNT")));
    }

    @Test
    public void shouldBeAbleToDeposit() throws Exception {
        Customer customer = customerService.insert(new Customer());
        BankAccount bankAccount = bankAccountService.newAccount(customer.getId(), AccountType.CHECKING_ACCOUNT);

        DepositDto depositDto = new DepositDto();
        depositDto.setIBAN(bankAccount.getIBAN());
        depositDto.setAmount(100D);

        MockHttpServletRequestBuilder mockRequest = post("/bank-account/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(depositDto));

        this.mockMvc.perform(mockRequest).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("CHECKING_ACCOUNT")))
                .andExpect(content().string(containsString("100")));
    }

    @Test
    public void shouldThrowErrorIfIbanNotFound() throws Exception {
        Customer customer = customerService.insert(new Customer());
        DepositDto depositDto = new DepositDto();
        depositDto.setIBAN("DE123");
        depositDto.setAmount(100D);

        MockHttpServletRequestBuilder mockRequest = post("/bank-account/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(depositDto));

        this.mockMvc.perform(mockRequest).andDo(print()).andExpect(status().is4xxClientError())
                .andExpect(content().string(containsString("Bank Account was not found")));
    }

    @Test
    public void shouldBeAbleToTransfer() throws Exception {
        Customer customer = customerService.insert(new Customer());
        BankAccount bankAccount1 = bankAccountService.newAccount(customer.getId(), AccountType.CHECKING_ACCOUNT);
        BankAccount bankAccount2 = bankAccountService.newAccount(customer.getId(), AccountType.CHECKING_ACCOUNT);
        bankAccountService.deposit(bankAccount1.getIBAN(), 500D);

        TransferDto transferDto = new TransferDto();
        transferDto.setFromIBAN(bankAccount1.getIBAN());
        transferDto.setToIBAN(bankAccount2.getIBAN());
        transferDto.setAmount(100D);

        MockHttpServletRequestBuilder mockRequest = post("/bank-account/transfer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(transferDto));

        this.mockMvc.perform(mockRequest).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("CHECKING_ACCOUNT")))
                .andExpect(content().string(containsString("400")));
    }

    @Test
    public void shouldThrowErrorOnInsufficientBalance() throws Exception {
        Customer customer = customerService.insert(new Customer());
        BankAccount bankAccount1 = bankAccountService.newAccount(customer.getId(), AccountType.CHECKING_ACCOUNT);
        BankAccount bankAccount2 = bankAccountService.newAccount(customer.getId(), AccountType.CHECKING_ACCOUNT);
        bankAccountService.deposit(bankAccount1.getIBAN(), 0D);

        TransferDto transferDto = new TransferDto();
        transferDto.setFromIBAN(bankAccount1.getIBAN());
        transferDto.setToIBAN(bankAccount2.getIBAN());
        transferDto.setAmount(100D);

        MockHttpServletRequestBuilder mockRequest = post("/bank-account/transfer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(transferDto));

        this.mockMvc.perform(mockRequest).andDo(print()).andExpect(status().is4xxClientError())
                .andExpect(content().string(containsString("Insufficient funds")));
    }

    @Test
    public void shouldGetInfo() throws Exception {
        Customer customer = customerService.insert(new Customer());
        BankAccount bankAccount = bankAccountService.newAccount(customer.getId(), AccountType.CHECKING_ACCOUNT);

        MockHttpServletRequestBuilder mockRequest = get("/bank-account/info")
                .contentType(MediaType.APPLICATION_JSON)
                .param("iban", bankAccount.getIBAN());

        this.mockMvc.perform(mockRequest).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("CHECKING_ACCOUNT")));
    }

    @Test
    public void shouldGetTransactions() throws Exception {
        Customer customer = customerService.insert(new Customer());
        BankAccount bankAccount = bankAccountService.newAccount(customer.getId(), AccountType.CHECKING_ACCOUNT);

        MockHttpServletRequestBuilder mockRequest = get("/bank-account/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .param("iban", bankAccount.getIBAN());

        this.mockMvc.perform(mockRequest).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("ACCOUNT_CREATION")));
    }


}
