package ir.morahimi061.codefactorybank.controller;

import ir.morahimi061.codefactorybank.dto.DepositDto;
import ir.morahimi061.codefactorybank.dto.ChangeStatusDto;
import ir.morahimi061.codefactorybank.dto.NewAccountDto;
import ir.morahimi061.codefactorybank.dto.TransferDto;
import ir.morahimi061.codefactorybank.model.BankAccount;
import ir.morahimi061.codefactorybank.model.Transaction;
import ir.morahimi061.codefactorybank.service.BankAccountService;
import ir.morahimi061.codefactorybank.service.TransactionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/bank-account")
public class BankAccountController {
    private final BankAccountService bankAccountService;
    private final TransactionService transactionService;

    public BankAccountController(BankAccountService bankAccountService, TransactionService transactionService) {
        this.bankAccountService = bankAccountService;
        this.transactionService = transactionService;
    }

    @PostMapping("/deposit")
    public ResponseEntity<BankAccount> deposit(@RequestBody DepositDto depositDto) {
        return ResponseEntity.ok(bankAccountService.deposit(depositDto.getIBAN(), depositDto.getAmount()));
    }

    @PostMapping("/transfer")
    public ResponseEntity<BankAccount> deposit(@RequestBody TransferDto transferDto) {
        return ResponseEntity.ok(bankAccountService.transfer(transferDto.getFromIBAN(), transferDto.getToIBAN(), transferDto.getAmount()));
    }

    @GetMapping("/info")
    public ResponseEntity<BankAccount> info(@RequestParam("iban") String iban) {
        return ResponseEntity.ok(bankAccountService.info(iban));
    }

    @GetMapping("/findAccount")
    public ResponseEntity<Set<BankAccount>> findAccount(@RequestBody NewAccountDto newAccountDto) {
        return ResponseEntity.ok(bankAccountService.findByAccountType(newAccountDto.getCustomerId(), newAccountDto.getAccountType()));
    }

    @GetMapping("/transactions")
    public ResponseEntity<Set<Transaction>> transactions(@RequestParam("iban") String iban) {
        return ResponseEntity.ok(transactionService.transactions(iban));
    }

    @PostMapping("/openAccount")
    public ResponseEntity<BankAccount> openAccount(@RequestBody NewAccountDto newAccountDto) {
        return ResponseEntity.ok(bankAccountService.newAccount(newAccountDto.getCustomerId(), newAccountDto.getAccountType()));
    }

    @PatchMapping("/changeStatus")
    public ResponseEntity<BankAccount> openAccount(@RequestBody ChangeStatusDto changeStatusDto) {
        return ResponseEntity.ok(bankAccountService.changeStatus(changeStatusDto.getIBAN(), changeStatusDto.isBlocked()));
    }
}
