package ir.morahimi061.codefactorybank.dto;

import lombok.Data;

@Data
//@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BankAccountView {
    private String IBAN;
    private Double balance;
}
