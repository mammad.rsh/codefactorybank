package ir.morahimi061.codefactorybank.dto;

import lombok.Data;

@Data
//@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ChangeStatusDto {
    private String IBAN;
    private boolean blocked;
}
