package ir.morahimi061.codefactorybank.dto;

import ir.morahimi061.codefactorybank.model.AccountType;
import lombok.Data;

@Data
//@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class NewAccountDto {
    private Long customerId;
    private AccountType accountType;
}
