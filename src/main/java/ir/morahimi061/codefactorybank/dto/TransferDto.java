package ir.morahimi061.codefactorybank.dto;

import lombok.Data;

@Data
//@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class TransferDto {
    private String fromIBAN;
    private String toIBAN;
    private Double amount;
}
