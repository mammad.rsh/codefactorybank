package ir.morahimi061.codefactorybank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodefactorybankApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodefactorybankApplication.class, args);
	}
}
