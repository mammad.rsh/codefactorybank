package ir.morahimi061.codefactorybank.repository;

import ir.morahimi061.codefactorybank.model.AccountType;
import ir.morahimi061.codefactorybank.model.BankAccount;
import ir.morahimi061.codefactorybank.model.exception.EntityNotFoundException;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {
    BankAccount findByIBAN(String IBAN);
    Set<BankAccount> findAllByAccountTypeAndCustomer_Id(AccountType type, Long id);
}
