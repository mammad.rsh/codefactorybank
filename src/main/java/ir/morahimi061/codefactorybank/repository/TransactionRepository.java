package ir.morahimi061.codefactorybank.repository;

import ir.morahimi061.codefactorybank.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    Set<Transaction> findAllByFrom_IBANOrTo_IBAN(String iban, String iban2);
}
