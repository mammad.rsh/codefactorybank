package ir.morahimi061.codefactorybank.service;

import ir.morahimi061.codefactorybank.model.exception.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import static org.springframework.beans.BeanUtils.copyProperties;

public abstract class CrudService<T, ID, Repository extends JpaRepository<T, ID>> {
    protected final Repository repository;
    protected final String entityName;

    protected CrudService(Repository repository, String entityName) {
        this.repository = repository;
        this.entityName = entityName;
    }

    public boolean existsById(ID id) {
        return repository.existsById(id);
    }

    public T findByIdOrFail(ID id) {
        var result = repository.findById(id);
        return result.orElseThrow(() -> new EntityNotFoundException(entityName, id));
    }

    public T insert(T object) {
        return repository.save(object);
    }

    public T updateOrFail(ID id, T newObject) {
        T oldObject = findByIdOrFail(id);
        copyProperties(newObject, oldObject, "id");
        return repository.save(oldObject);
    }


    public void deleteOrFail(ID id) {
        if (!repository.existsById(id))
            entityNotFoundException("id", id);
        repository.deleteById(id);
    }

    protected void entityNotFoundException(String field, Object fieldValue) {
        throw new EntityNotFoundException(entityName, field, fieldValue.toString());
    }


}
