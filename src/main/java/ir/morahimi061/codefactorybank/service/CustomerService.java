package ir.morahimi061.codefactorybank.service;

import ir.morahimi061.codefactorybank.model.Customer;
import ir.morahimi061.codefactorybank.repository.CustomerRepository;
import org.springframework.stereotype.Service;

@Service
public class CustomerService extends CrudService<Customer, Long, CustomerRepository> {
    protected CustomerService(CustomerRepository repository) {
        super(repository, "Customer");
    }
}
