package ir.morahimi061.codefactorybank.service;

import ir.morahimi061.codefactorybank.dto.ChangeStatusDto;
import ir.morahimi061.codefactorybank.model.AccountType;
import ir.morahimi061.codefactorybank.model.BankAccount;
import ir.morahimi061.codefactorybank.model.Customer;
import ir.morahimi061.codefactorybank.model.TransactionType;
import ir.morahimi061.codefactorybank.model.exception.ActionNotAllowedException;
import ir.morahimi061.codefactorybank.model.exception.EntityNotFoundException;
import ir.morahimi061.codefactorybank.model.exception.ErrorCode;
import ir.morahimi061.codefactorybank.repository.BankAccountRepository;
import ir.morahimi061.codefactorybank.repository.CustomerRepository;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.Set;

@Service
public class BankAccountService extends CrudService<BankAccount, Long, BankAccountRepository> {
    private final TransactionService transactionService;
    private final CustomerService customerService;

    @Autowired
    protected BankAccountService(BankAccountRepository repository, TransactionService transactionService, CustomerService customerService) {
        super(repository, "Bank Account");
        this.transactionService = transactionService;
        this.customerService = customerService;
    }

    public BankAccount deposit(String IBAN, Double amount) {
        BankAccount bankAccount = repository.findByIBAN(IBAN);
        if (null != bankAccount) {
            isBlocked(bankAccount);
            Double newBalance = bankAccount.getBalance() + amount;
            bankAccount.setBalance(newBalance);
            repository.save(bankAccount);

            transactionService.newTransaction(TransactionType.DEPOSIT, null, bankAccount, amount);
            return bankAccount;
        }
        throw new EntityNotFoundException(entityName, "IBAN", IBAN);
    }

    public BankAccount transfer(String fromIBAN, String toIBAN, Double amount) {
        BankAccount fromBankAccount = repository.findByIBAN(fromIBAN);
        BankAccount toBankAccount = repository.findByIBAN(toIBAN);
        if (null == fromBankAccount)
            throw new EntityNotFoundException(entityName, "IBAN", fromIBAN);

        if (null == toBankAccount)
            throw new EntityNotFoundException(entityName, "IBAN", toIBAN);

        isBlocked(fromBankAccount);
        isBlocked(toBankAccount);

        if (fromBankAccount.getAccountType() == AccountType.SAVINGS_ACCOUNT)
            if (toBankAccount.getAccountType() != AccountType.CHECKING_ACCOUNT)
                throw new ActionNotAllowedException("Only transferring\n" +
                        "money from the savings account to the reference account (checking account) is possible.", ErrorCode.TRANSFER_NOT_POSSIBLE);

        if (fromBankAccount.getAccountType() == AccountType.PRIVATE_LOAN_ACCOUNT)
            throw new ActionNotAllowedException("Withdrawal is not possible from Private loan account.", ErrorCode.WITHDRAWAL_NOT_POSSIBLE);

        double newFromBalance = fromBankAccount.getBalance() - amount;
        if (newFromBalance < 0)
            throw new ActionNotAllowedException("Insufficient funds!", ErrorCode.INSUFFICIENT_FUNDS);
        double newToBalance = toBankAccount.getBalance() + amount;
        // better way is to make withdrawal and deposit two separate functions for more control
        fromBankAccount.setBalance(newFromBalance);
        toBankAccount.setBalance(newToBalance);
        repository.save(fromBankAccount);
        repository.save(toBankAccount);

        transactionService.newTransaction(TransactionType.TRANSFER, fromBankAccount, toBankAccount, amount);

        return fromBankAccount;
    }

    public BankAccount info(String iban) {
        BankAccount bankAccount = repository.findByIBAN(iban);
        if (null != bankAccount) {
            return bankAccount;
        }
        throw new EntityNotFoundException(entityName, "IBAN", iban);
    }

    public Set<BankAccount> findByAccountType(Long customerId, AccountType type) {
        return repository.findAllByAccountTypeAndCustomer_Id(type, customerId);
    }

    public BankAccount newAccount(Long customerId, AccountType type) {
        Customer customer = customerService.findByIdOrFail(customerId);
        Random rand = new Random();
        StringBuilder iban = new StringBuilder("DE");
        for (int i = 0; i < 20; i++)
        {
            int n = rand.nextInt(10);
            iban.append(Integer.toString(n));
        }
        BankAccount bankAccount = insert(new BankAccount(type, iban.toString(), customer));
        transactionService.newTransaction(TransactionType.ACCOUNT_CREATION, bankAccount, null, 0);

        return bankAccount;
    }

    public BankAccount changeStatus(String iban, boolean blocked) {
        BankAccount bankAccount = repository.findByIBAN(iban);
        bankAccount.setBlocked(blocked);
        repository.save(bankAccount);

        return bankAccount;
    }

    public void isBlocked(BankAccount bankAccount) {
        if (bankAccount.isBlocked())
            throw new ActionNotAllowedException(bankAccount.getIBAN() + " is Blocked!", ErrorCode.ACCOUNT_IS_BLOCKED);
    }

}
