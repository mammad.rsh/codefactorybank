package ir.morahimi061.codefactorybank.service;

import ir.morahimi061.codefactorybank.model.BankAccount;
import ir.morahimi061.codefactorybank.model.Transaction;
import ir.morahimi061.codefactorybank.model.TransactionType;
import ir.morahimi061.codefactorybank.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Set;

@Service
public class TransactionService extends CrudService<Transaction, Long, TransactionRepository> {

    @Autowired
    protected TransactionService(TransactionRepository repository) {
        super(repository, "Transaction");
    }

    public void newTransaction(TransactionType type, BankAccount from, BankAccount to, double amount) {
        Transaction transaction = new Transaction(type, from, to, amount, LocalDateTime.now());
        repository.save(transaction);
    }

    public Set<Transaction> transactions(String iban) {
        return repository.findAllByFrom_IBANOrTo_IBAN(iban, iban);
    }
}
