package ir.morahimi061.codefactorybank.model;

public enum TransactionType {
    DEPOSIT,
    WITHDRAWAL,
    TRANSFER,
    ACCOUNT_CREATION
}
