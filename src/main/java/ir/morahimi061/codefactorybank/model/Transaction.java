package ir.morahimi061.codefactorybank.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    TransactionType type;
    double amount;
    LocalDateTime date;

    @ManyToOne
    private BankAccount from;
    @ManyToOne
    private BankAccount to;

    public Transaction(TransactionType type, BankAccount from, BankAccount to, double amount, LocalDateTime date) {
        this.type = type;
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.date = date;
    }

    public Transaction() {

    }
}
