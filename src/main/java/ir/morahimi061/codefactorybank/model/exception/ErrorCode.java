package ir.morahimi061.codefactorybank.model.exception;

public enum ErrorCode {
    INSUFFICIENT_FUNDS,
    WITHDRAWAL_NOT_POSSIBLE,
    TRANSFER_NOT_POSSIBLE,
    ACCOUNT_IS_BLOCKED
}
