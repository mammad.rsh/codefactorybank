package ir.morahimi061.codefactorybank.model;


public enum AccountType {
    CHECKING_ACCOUNT,
    SAVINGS_ACCOUNT,
    PRIVATE_LOAN_ACCOUNT
}
