package ir.morahimi061.codefactorybank.model;

import lombok.Data;
import lombok.Value;
import net.bytebuddy.implementation.bind.annotation.Default;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private AccountType accountType;
    private String IBAN;
    private Double balance;
    private boolean blocked = false; // 1 Blocked

    @ManyToOne
    private Customer customer;

    @OneToMany
    private Set<Transaction> transactions;

    public BankAccount(AccountType type, String IBAN, Customer customer) {
        this.accountType = type;
        this.IBAN = IBAN;
        this.customer = customer;
        this.balance = 0D;
    }

    public BankAccount() {

    }
}
